package com.example.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    private Button btnNext;
    private EditText etName,etAccount,etPassword,etCheckpassword,etRealname,etID,etPhone,etEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        btnNext = findViewById(R.id.btnNext);
        etName = findViewById(R.id.etName);
        etAccount = findViewById(R.id.etAccount);
        etPassword = findViewById(R.id.etPassword);
        etCheckpassword = findViewById(R.id.etCheckpassword);
        etRealname = findViewById(R.id.etRealname);
        etID = findViewById(R.id.etID);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SignupActivity.this,"btnNext",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(SignupActivity.this, VerifcationActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
}
